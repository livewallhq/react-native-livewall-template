// @flow strict

import thunk from "redux-thunk";
import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from "./reducers/rootReducer";

let middleware = [thunk];
const composeEnhancers  = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let enhancer = composeEnhancers(applyMiddleware(...middleware));

export default function configureStore(initialState: {} = {}) {
    return createStore(rootReducer, initialState, enhancer);
}