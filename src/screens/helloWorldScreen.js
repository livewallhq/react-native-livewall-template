// @flow strict
import React, {Component} from 'react'
import {StyleSheet, Text, SafeAreaView} from 'react-native';

type Props = {
    componentId: number,
}

export default class HelloWorldScreen extends Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.hello}>Hello World!</Text>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
    },
    hello: {
        fontSize: 24,
        fontWeight: 'bold'
    }
});