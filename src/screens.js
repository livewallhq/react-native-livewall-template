// @flow strict
import {Navigation} from "react-native-navigation";
import {helloWorldScreen} from "./constants/screens";
import HelloWorldScreen from "./screens/helloWorldScreen";

// $FlowFixMe
export function registerScreens(store, provider: Provider) {
    Navigation.registerComponentWithRedux(helloWorldScreen, () => HelloWorldScreen, provider, store);
}