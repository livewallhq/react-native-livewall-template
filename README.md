To create a new project using this template, run the following command:
```
react-native init <yourprojectname> --template git+https://<yourbitbucketusername>@bitbucket.org/livewallhq/react-native-livewall-template
```

After creating a new project, copy the config below in the package.json file. We cannot put stuff in the package.json from the template so this will have to do.

```
"jest": {
    "collectCoverage": true,
    "coveragePathIgnorePatterns": [
      "/node_modules/",
      "/assets/",
      "/constants/",
      "actiontypes.js",
      "dummydata.js",
      "index.js",
      "initialState.js"
    ],
    "preset": "react-native",
    "transform": {
      "^.+\\.(js)$": "<rootDir>/node_modules/react-native/jest/preprocessor.js"
    },
    "setupTestFrameworkScriptFile": "./node_modules/jest-enzyme/lib/index.js",
    "setupFiles": [
      "enzyme-react-16-adapter-setup"
    ]
  }
```

### Things to do after init

1. Configure react-native-navigation, click [here](https://wix.github.io/react-native-navigation/v2/#/docs/Installing) to see how
2. Examine `.env` files for react-native-config
3. Run new app

### If you are using the React Native Debugger
Don't forget to install it and add ```"postinstall": "rndebugger-open"```, to the scripts section of your package.json.

#Custom fonts
If you are using custom fonts, don't forget to add ```"rnpm": {
                                                          "assets": [
                                                            "./fonts"
                                                          ]
                                                        }``` to your package.json.